use std::fmt;
use std::io;
use std::marker::PhantomData;

pub mod cellular_process;

/// Printer is the type of a printer function, which takes a string and prints it (to stdout, a file, whatever)
pub type Printer = Fn(&str) -> ();
/// Reader is the type of a reader function, which returns a string (from stdin, a file, whatever)
pub type Reader = Fn() -> String;

/// Trait CellularAutomata is a trait that all cellular automata must satisfy
pub trait CellularAutomata<'a, CellDataType> {
    /// Function run runs the cellular automata
    fn run(&mut self, &'a str) -> ();
    /// Function new creates a new, empty cellular automata
    fn new() -> Self;
    /// Function input_handler takes the grid and a reader and modifies the grid according to what it reads. The printer argument is there just so the automata can prompt the user (if needed)
    fn input_handler(&mut cellular_process::Grid<CellDataType>, &Reader, &Printer) -> ();
    /// Function output_handler takes a grid and a printer and prints out something, according to the data of the grid.
    fn output_handler(&cellular_process::Grid<CellDataType>, &Printer) -> ();
}

/// DummyCellularAutomata is just a dummy cellular automata, which simply prints "It works!"
pub struct DummyCellularAutomata<'a> {
    phantom: PhantomData<&'a str>,
}

/// UsizeWrapper is a wrapper for usize satisfying certain traits, such as From::<String>()
#[derive(Clone)]
pub struct UsizeWrapper {
    data: usize,
}

impl From<String> for UsizeWrapper {
    fn from(data: String) -> Self {
        UsizeWrapper {
            data: data.parse::<usize>().unwrap(),
        }
    }
}

impl fmt::Display for UsizeWrapper {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.data)
    }
}

/// StaticCellularAutomata is an implementation of a static cellular automata (which remains the same)
pub struct StaticCellularAutomata<'a> {
    phantom: PhantomData<&'a str>,
}

impl<'a> StaticCellularAutomata<'a> {
    /// printer is the printer function
    pub fn printer(input: &str) {
        println!("{}", input);
    }
    /// reader is the reader function
    #[allow(dead_code)]
    pub fn reader() -> String {
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("Couldn't read user input");
        input
    }
    pub fn neighbor_parser(
        data: &cellular_process::GridDataType<UsizeWrapper>,
        address: usize,
    ) -> cellular_process::NeighborType<UsizeWrapper> {
        cellular_process::NeighborType::new(vec![&data.data[address]], &data.dimensions)
    }
    pub fn transition(
        data: &mut UsizeWrapper,
        neighbor: &cellular_process::NeighborType<UsizeWrapper>,
    ) -> () {

    }
    pub fn run(
        exec: &cellular_process::ExecFunc<UsizeWrapper>,
        transition: &cellular_process::TransitionFunc<UsizeWrapper>,
        neighbor: &cellular_process::NeighborFuncParser<UsizeWrapper>,
        low: usize,
        high: usize,
        write: &mut [UsizeWrapper],
        read: &cellular_process::GridDataType<UsizeWrapper>,
    ) {
        exec(transition, neighbor, low, high, write, read);
    }
}

impl<'a> CellularAutomata<'a, UsizeWrapper> for StaticCellularAutomata<'a> {
    fn new() -> StaticCellularAutomata<'a> {
        StaticCellularAutomata {
            phantom: PhantomData,
        }
    }
    fn input_handler(
        grid: &mut cellular_process::Grid<UsizeWrapper>,
        reader: &Reader,
        printer: &Printer,
    ) -> () {
        printer(&String::from("Read keyboard input"));
    }
    fn output_handler(grid: &cellular_process::Grid<UsizeWrapper>, printer: &Printer) {
        let data_read = grid.get_data();
        let data_parsed = data_read.get_data();
        let dimensions_parsed = data_read.get_dimensions();
        for (i, temp) in data_parsed.iter().enumerate() {
            printer(&temp.to_string());
            if i % dimensions_parsed[0] == 0 {
                printer("\n");
            }
        }
    }
    fn run(&mut self, input: &'a str) {
        let mut grid: cellular_process::Grid<UsizeWrapper> = cellular_process::Grid::new();
        grid.read_from_str(input);
        grid.apply(&Self::transition, &Self::neighbor_parser, &Self::run, 1);
        Self::output_handler(&grid, &Self::printer);
    }
}

impl<'a> DummyCellularAutomata<'a> {
    /// printer is the printer function
    fn printer(input: &str) {
        println!("{}", input);
    }
    /// reader is the reader function
    #[allow(dead_code)]
    fn reader() -> String {
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("Couldn't read user input");
        input
    }
}

impl<'a> CellularAutomata<'a, String> for DummyCellularAutomata<'a> {
    fn run(&mut self, input: &'a str) {
        let temp: Vec<usize> = Vec::new();
        let g: cellular_process::Grid<String> = cellular_process::Grid::new();
        Self::output_handler(&g, &Self::printer);
        Self::printer(&String::from("Recieved input:"));
        Self::printer(input);
    }
    fn new() -> DummyCellularAutomata<'a> {
        DummyCellularAutomata {
            phantom: PhantomData,
        }
    }
    fn input_handler(
        _grid: &mut cellular_process::Grid<String>,
        _reader: &Reader,
        printer: &Printer,
    ) {
        printer(&String::from("Read keyboard input"));
    }
    fn output_handler(_grid: &cellular_process::Grid<String>, printer: &Printer) {
        printer(&String::from("It works!"));
    }
}
