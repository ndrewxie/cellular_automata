use std::marker::PhantomData;

#[derive(Clone)]
/// GridDataType is the datatype of the data inside of a grid (the struct Grid is just a wrapper around GridDataType with a few more functions)
pub struct GridDataType<CellDataType> {
    /// Data is just the data of the grid. The data is "flattened" to 1 dimension, no matter how many dimensions the grid is
    pub data: Vec<CellDataType>,
    /// Dimensions is a vector that stores the dimensions of the data - for example, a 2x2 grid (a 2d grid, with 2 rows and 2 columns) would be [2, 2]. The convention is that the size of x is before the size of y before the size of z etc, so a 3d grid with length (x size) 3, width (y size) 4 and height (z size) 5 would be [3,4,5]
    pub dimensions: Vec<usize>,
}

#[derive(Clone)]
/// NeighborType is the datatype ofr the neighborhood of a cell
pub struct NeighborType<'a, CellDataType> {
    /// Data contains the neighborhood
    pub data: Vec<&'a CellDataType>,
    /// Dimensions is the dimensions
    pub dimensions: &'a Vec<usize>,
}

/// TransitionFunc is the transition function (AKA "rule") of the cellular automata
pub type TransitionFunc<CellDataType> = Fn(&mut CellDataType, &NeighborType<CellDataType>) -> ();

/// NeighborFuncParser is the function that returns the neighborhood of a cell
pub type NeighborFuncParser<CellDataType> =
    Fn(&GridDataType<CellDataType>, usize) -> NeighborType<CellDataType>;

/*
/// AtFunc is the function that returns the value of the cell at a given index
pub type AtFunc<CellDataType> = Fn(&GridDataType<CellDataType>, usize) -> CellDataType;

/// SetFunc is the function that sets a given address's data in a grid with a given value
pub type SetFunc<CellDataType> = Fn(&mut GridDataType<CellDataType>, usize, &CellDataType) -> ();
*/

/// ExecFunc is an internal function that does the computation for a "chunk" of data - each chunk is done in parallel.
pub type ExecFunc<CellDataType> = Fn(
    &TransitionFunc<CellDataType>,
    &NeighborFuncParser<CellDataType>,
    usize,
    usize,
    &mut [CellDataType],
    &GridDataType<CellDataType>,
) -> ();

/// RunFunc is the function that runs a function (of type ExecFunc) on a seperate thread (or, if there are no threads, simply runs the function)
pub type RunFunc<CellDataType> = Fn(
    &ExecFunc<CellDataType>,
    &TransitionFunc<CellDataType>,
    &NeighborFuncParser<CellDataType>,
    usize,
    usize,
    &mut [CellDataType],
    &GridDataType<CellDataType>,
) -> ();

/// HaltFunc is the function that tells the program whether to halt or not (true = halt, false = not halt, obviously)
pub type HaltFunc<CellType> = Fn(&GridDataType<CellType>) -> bool;

/// Grid is the datatype for a Grid object, where all of the cells are stored
pub struct Grid<'a, CellDataType> {
    /// Direction determines which data (data_a or data_b) will be read from, and which data will be written to (the data being read from is never the data being written to). Direction "flops" each cycle. True = write to (modify) data_a, read from data_b (so data_a is ALWAYS the most recent one), false = other way around.
    direction: bool,
    /// Data_a contains the vector. Depending on direction, it is either read to or written to.
    data_a: GridDataType<CellDataType>,
    /// Data_a contains the vector. Depending on direction, it is either read to or written to.
    data_b: GridDataType<CellDataType>,
    /// Phantom is just Phantom. Don't touch it or it will haunt you for eternity.
    phantom: PhantomData<&'a str>,
}

impl<'a, CellDataType: 'static + Clone + ToString + From<String>> Grid<'a, CellDataType> {
    /// Function new creates a blank Grid
    pub fn new() -> Grid<'a, CellDataType> {
        let temp_grid_data: GridDataType<CellDataType> = GridDataType {
            data: Vec::new(),
            dimensions: Vec::new(),
        };
        Grid {
            data_a: temp_grid_data.clone(),
            data_b: temp_grid_data.clone(),
            phantom: PhantomData,
            direction: true,
        }
    }
    /// Function get_data gets the most recent data from the struct
    pub fn get_data(&self) -> GridDataType<CellDataType> {
        if self.direction == true {
            self.data_a.clone()
        } else {
            self.data_b.clone()
        }
    }
    /// Function read_from_string reads a Grid from a string - mutating the object
    pub fn read_from_str(&mut self, data: &str) {
        *self = Self::decode(data);
    }
    /// Function decode decodes the data into a Grid type. INTERNAL.
    pub fn decode(input: &str) -> Grid<'a, CellDataType> {
        let split: Vec<&str> = input.split(';').collect(); // Tokenize by semicolons, which seperates the data from the "metadata" - the "metadata" are things like the dimensions, and the "data" is the actual cell state
        if split.len() != 2 {
            // as of now, there are 2 "big tokens" (seperated by semicolons) - the first is the dimensions, and the second is the data.
            println!("ERROR - malformed input file.");
            panic!("ERROR - malformed input file.");
        }
        let split_dimensions: Vec<&str> = split[0].split(',').collect(); // Each "big token" has "smaller tokens", seperated by commas. In this case, the comma seperates the sizes of the grid in each dimension
        let mut dimensions: Vec<usize> = Vec::new();
        for j in split_dimensions {
            dimensions.push(String::from(j).parse::<usize>().unwrap());
            //dimensions.push(j.parse::<usize>().unwrap());
        }
        let split_data: Vec<&str> = split[1].split(',').collect(); // Same as previous, but this time for the actual data, not the dimensions
        let mut data: Vec<CellDataType> = Vec::new();
        for j in split_data {
            data.push(CellDataType::from(String::from(j)));
        }
        let generated_data: GridDataType<CellDataType> = GridDataType {
            data: data.clone(),
            dimensions: dimensions.clone(),
        };
        Grid {
            data_a: generated_data.clone(),
            data_b: generated_data.clone(),
            phantom: PhantomData,
            direction: true,
        }
    }
    /// Function encode encodes the Grid in a string. INTERNAL
    pub fn encode(&self) -> String {
        let mut to_return: String = String::new();
        let active_data: GridDataType<CellDataType> = {
            if self.direction {
                self.data_a.clone()
            } else {
                self.data_b.clone()
            }
        };
        for j in active_data.dimensions {
            to_return += &j.to_string();
            to_return += ",";
        }
        to_return += ";";
        for j in active_data.data {
            to_return += &j.to_string();
            to_return += ",";
        }
        to_return
    }
    /// Function from_str parses a Grid from a string. Does not mutate the object.
    pub fn from_str(data: &str) -> Grid<'a, CellDataType> {
        let mut temp = Grid::new();
        temp.read_from_str(data);
        temp
    }
    /// compute_internal is an internal computation function. Transition is the transition function, neighbor_parser is the function that returns the neighborhood of a cell, address_low and address_high are the low and high addresses of the slice, respectively (this function is given a "slice" of data), to_write is a mutable reference to the slice to write to, and to_read is an immutable reference to the slice to read from
    fn compute_internal(
        transition: &TransitionFunc<CellDataType>,
        neighbor_parser: &NeighborFuncParser<CellDataType>,
        address_low: usize,
        address_high: usize,
        to_write: &mut [CellDataType],
        to_read: &GridDataType<CellDataType>,
    ) {
        for j in address_low..address_high {
            transition(
                &mut to_write[j - address_low],
                &neighbor_parser(&to_read, j),
            );
        }
    }
    /// Function apply performs one iteration
    pub fn apply(
        &mut self,
        transition: &'a TransitionFunc<CellDataType>,
        neighbor_parser: &'a NeighborFuncParser<CellDataType>,
        run: &'a RunFunc<CellDataType>,
        distribute_size: usize,
    ) {
        let (to_modify, to_read): (&mut GridDataType<CellDataType>, &GridDataType<CellDataType>) = {
            if self.direction {
                (&mut self.data_a, &self.data_b)
            } else {
                (&mut self.data_b, &self.data_a)
            }
        };
        let chunk_size: usize = distribute_size ^ (to_read.dimensions.len());
        for j in 0..(to_read.data.len() % chunk_size) {
            let (head, _): (&mut [CellDataType], &mut [CellDataType]) =
                to_modify.data.split_at_mut(chunk_size * (j + 1));
            run(
                &Self::compute_internal,
                transition,
                neighbor_parser,
                j * chunk_size,
                (j + 1) * chunk_size,
                head,
                &to_read,
            );
        }
    }
}

impl<CellDataType: Clone> GridDataType<CellDataType> {
    pub fn new(dimensions: &[usize]) -> GridDataType<CellDataType> {
        GridDataType {
            data: Vec::new(),
            dimensions: dimensions.to_vec(),
        }
    }
    pub fn from_data(data: &[CellDataType], dimensions: &[usize]) -> GridDataType<CellDataType> {
        GridDataType {
            data: data.to_vec(),
            dimensions: dimensions.to_vec(),
        }
    }
    pub fn get_data(&self) -> Vec<CellDataType> {
        self.data.clone()
    }
    pub fn get_dimensions(&self) -> Vec<usize> {
        self.dimensions.clone()
    }
}

impl<'a, CellDataType: Clone> NeighborType<'a, CellDataType> {
    pub fn new(
        data: Vec<&'a CellDataType>,
        dimensions: &'a Vec<usize>,
    ) -> NeighborType<'a, CellDataType> {
        NeighborType {
            data: data,
            dimensions: &dimensions,
        }
    }
}
