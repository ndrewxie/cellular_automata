use std::env;
use std::fs::File;
use std::io::prelude::*;

extern crate cellular_lib;

fn read_file(filename: &str) -> String {
    let mut file = File::open(filename).expect("Unable to open the file");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Unable to read the file");
    contents
}

fn main() {
    let arguments: Vec<String> = env::args().collect();
    if arguments.len() == 2 {
        let contents = read_file(&arguments[1]);
        let mut automata: cellular_lib::cellular_automata::StaticCellularAutomata =
            cellular_lib::cellular_automata::CellularAutomata::new();
        cellular_lib::cellular_automata::CellularAutomata::run(&mut automata, &contents);
    } else {
        println!(
            "USAGE - CVM <FileName>, where <FileName> is the name of the file to be executed."
        );
    }
}
